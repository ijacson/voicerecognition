package br.com.ijksystems.voicerecognition

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.widget.Button
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import br.com.ijksystems.voicerecognition.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val btnFalar: Button = binding.btnFalar
        val btnLimpar: Button = binding.btnLimpar
        btnFalar.setOnClickListener {
            limparVozReconhecida()
            acionarReconhecimentoDeVoz()
        }

        btnLimpar.setOnClickListener {
            limparVozReconhecida()
        }


    }

    private fun limparVozReconhecida() {
        binding.edtVozReconhecida.setText("")
    }

    private var pegarResultadoDaVoz =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                binding.edtVozReconhecida.setText(data?.get(0))
            }

        }

    fun acionarReconhecimentoDeVoz() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )

        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Diga alguma coisa!")

        pegarResultadoDaVoz.launch(intent)
    }
}